//CHECKSUM:840bf8f30e6ccf2104ae92387c0569b0fd156ce1eb3af65d7e3c0838f43311a3
'use strict';

const _ = require('lodash');

const INTENT_PREFIX = 'intent:';
/**
 * Get a variable under this user's storage
 * @title Validate user choice
 * @category Skills
 * @hidden true
 * @author Botpress, Inc.
 * @param {string} data - The parameters of the available choices
 */

const validateChoice = async data => {
  let choice = undefined;
  const config = await bp.config.getModuleConfigForBot('basic-skills', event.botId);

  const nb = _.get(event.preview.match(/^[#).!]?([\d]{1,2})[#).!]?$/), '[1]');

  if (config.matchNumbers && nb) {
    const index = parseInt(nb) - 1;
    const element = await bp.cms.getContentElement(event.botId, data.contentId);
    choice = _.get(element, `formData.choices.${index}.value`);
  }

  if (!choice && config.matchNLU) {
    choice = _.findKey(data.keywords, keywords => {
      const intents = keywords.filter(x => (x || '').toLowerCase().startsWith(INTENT_PREFIX)).map(x => x.substr(INTENT_PREFIX.length));
      return _.some(intents, k => event.nlu.intent.name === k);
    });
  }

  if (!choice) {
    choice = _.findKey(data.keywords, keywords => _.some(keywords || [], k => _.includes((event.preview || '').toLowerCase(), (k || '').toLowerCase()) || event.payload && _.includes(_.get(event, 'payload.text', '').toLowerCase(), (k || '').toLowerCase())));
  }

  if (choice) {
    temp['skill-choice-valid'] = true;
    temp['skill-choice-ret'] = choice;
  } else {
    temp['skill-choice-valid'] = false;
  }
};

return validateChoice(args);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNob2ljZV9wYXJzZV9hbnN3ZXIuanMiXSwibmFtZXMiOlsiXyIsInJlcXVpcmUiLCJJTlRFTlRfUFJFRklYIiwidmFsaWRhdGVDaG9pY2UiLCJkYXRhIiwiY2hvaWNlIiwidW5kZWZpbmVkIiwiY29uZmlnIiwiYnAiLCJnZXRNb2R1bGVDb25maWdGb3JCb3QiLCJldmVudCIsImJvdElkIiwibmIiLCJnZXQiLCJwcmV2aWV3IiwibWF0Y2giLCJtYXRjaE51bWJlcnMiLCJpbmRleCIsInBhcnNlSW50IiwiZWxlbWVudCIsImNtcyIsImdldENvbnRlbnRFbGVtZW50IiwiY29udGVudElkIiwibWF0Y2hOTFUiLCJmaW5kS2V5Iiwia2V5d29yZHMiLCJpbnRlbnRzIiwiZmlsdGVyIiwieCIsInRvTG93ZXJDYXNlIiwic3RhcnRzV2l0aCIsIm1hcCIsInN1YnN0ciIsImxlbmd0aCIsInNvbWUiLCJrIiwibmx1IiwiaW50ZW50IiwibmFtZSIsImluY2x1ZGVzIiwicGF5bG9hZCIsInRlbXAiLCJhcmdzIl0sIm1hcHBpbmdzIjoiQUFBQTs7QUFDQSxNQUFNQSxDQUFDLEdBQUdDLE9BQU8sQ0FBQyxRQUFELENBQWpCOztBQUNBLE1BQU1DLGFBQWEsR0FBRyxTQUF0QjtBQUVBOzs7Ozs7Ozs7QUFRQSxNQUFNQyxjQUFjLEdBQUcsTUFBTUMsSUFBTixJQUFjO0FBQ25DLE1BQUlDLE1BQU0sR0FBR0MsU0FBYjtBQUNBLFFBQU1DLE1BQU0sR0FBRyxNQUFNQyxFQUFFLENBQUNELE1BQUgsQ0FBVUUscUJBQVYsQ0FBZ0MsY0FBaEMsRUFBZ0RDLEtBQUssQ0FBQ0MsS0FBdEQsQ0FBckI7O0FBQ0EsUUFBTUMsRUFBRSxHQUFHWixDQUFDLENBQUNhLEdBQUYsQ0FBTUgsS0FBSyxDQUFDSSxPQUFOLENBQWNDLEtBQWQsQ0FBb0IsNkJBQXBCLENBQU4sRUFBMEQsS0FBMUQsQ0FBWDs7QUFFQSxNQUFJUixNQUFNLENBQUNTLFlBQVAsSUFBdUJKLEVBQTNCLEVBQStCO0FBQzdCLFVBQU1LLEtBQUssR0FBR0MsUUFBUSxDQUFDTixFQUFELENBQVIsR0FBZSxDQUE3QjtBQUNBLFVBQU1PLE9BQU8sR0FBRyxNQUFNWCxFQUFFLENBQUNZLEdBQUgsQ0FBT0MsaUJBQVAsQ0FBeUJYLEtBQUssQ0FBQ0MsS0FBL0IsRUFBc0NQLElBQUksQ0FBQ2tCLFNBQTNDLENBQXRCO0FBQ0FqQixJQUFBQSxNQUFNLEdBQUdMLENBQUMsQ0FBQ2EsR0FBRixDQUFNTSxPQUFOLEVBQWdCLG9CQUFtQkYsS0FBTSxRQUF6QyxDQUFUO0FBQ0Q7O0FBRUQsTUFBSSxDQUFDWixNQUFELElBQVdFLE1BQU0sQ0FBQ2dCLFFBQXRCLEVBQWdDO0FBQzlCbEIsSUFBQUEsTUFBTSxHQUFHTCxDQUFDLENBQUN3QixPQUFGLENBQVVwQixJQUFJLENBQUNxQixRQUFmLEVBQXlCQSxRQUFRLElBQUk7QUFDNUMsWUFBTUMsT0FBTyxHQUFHRCxRQUFRLENBQ3JCRSxNQURhLENBQ05DLENBQUMsSUFBSSxDQUFDQSxDQUFDLElBQUksRUFBTixFQUFVQyxXQUFWLEdBQXdCQyxVQUF4QixDQUFtQzVCLGFBQW5DLENBREMsRUFFYjZCLEdBRmEsQ0FFVEgsQ0FBQyxJQUFJQSxDQUFDLENBQUNJLE1BQUYsQ0FBUzlCLGFBQWEsQ0FBQytCLE1BQXZCLENBRkksQ0FBaEI7QUFHQSxhQUFPakMsQ0FBQyxDQUFDa0MsSUFBRixDQUFPUixPQUFQLEVBQWdCUyxDQUFDLElBQUl6QixLQUFLLENBQUMwQixHQUFOLENBQVVDLE1BQVYsQ0FBaUJDLElBQWpCLEtBQTBCSCxDQUEvQyxDQUFQO0FBQ0QsS0FMUSxDQUFUO0FBTUQ7O0FBRUQsTUFBSSxDQUFDOUIsTUFBTCxFQUFhO0FBQ1hBLElBQUFBLE1BQU0sR0FBR0wsQ0FBQyxDQUFDd0IsT0FBRixDQUFVcEIsSUFBSSxDQUFDcUIsUUFBZixFQUF5QkEsUUFBUSxJQUN4Q3pCLENBQUMsQ0FBQ2tDLElBQUYsQ0FDRVQsUUFBUSxJQUFJLEVBRGQsRUFFRVUsQ0FBQyxJQUNDbkMsQ0FBQyxDQUFDdUMsUUFBRixDQUFXLENBQUM3QixLQUFLLENBQUNJLE9BQU4sSUFBaUIsRUFBbEIsRUFBc0JlLFdBQXRCLEVBQVgsRUFBZ0QsQ0FBQ00sQ0FBQyxJQUFJLEVBQU4sRUFBVU4sV0FBVixFQUFoRCxLQUNDbkIsS0FBSyxDQUFDOEIsT0FBTixJQUFpQnhDLENBQUMsQ0FBQ3VDLFFBQUYsQ0FBV3ZDLENBQUMsQ0FBQ2EsR0FBRixDQUFNSCxLQUFOLEVBQWEsY0FBYixFQUE2QixFQUE3QixFQUFpQ21CLFdBQWpDLEVBQVgsRUFBMkQsQ0FBQ00sQ0FBQyxJQUFJLEVBQU4sRUFBVU4sV0FBVixFQUEzRCxDQUp0QixDQURPLENBQVQ7QUFRRDs7QUFFRCxNQUFJeEIsTUFBSixFQUFZO0FBQ1ZvQyxJQUFBQSxJQUFJLENBQUMsb0JBQUQsQ0FBSixHQUE2QixJQUE3QjtBQUNBQSxJQUFBQSxJQUFJLENBQUMsa0JBQUQsQ0FBSixHQUEyQnBDLE1BQTNCO0FBQ0QsR0FIRCxNQUdPO0FBQ0xvQyxJQUFBQSxJQUFJLENBQUMsb0JBQUQsQ0FBSixHQUE2QixLQUE3QjtBQUNEO0FBQ0YsQ0FyQ0Q7O0FBdUNBLE9BQU90QyxjQUFjLENBQUN1QyxJQUFELENBQXJCIiwic291cmNlUm9vdCI6Ii92YXIvbGliL2plbmtpbnMvd29ya3NwYWNlL2J1aWxkLWxpbnV4L21vZHVsZXMvYmFzaWMtc2tpbGxzL3NyYy9iYWNrZW5kIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnXG5jb25zdCBfID0gcmVxdWlyZSgnbG9kYXNoJylcbmNvbnN0IElOVEVOVF9QUkVGSVggPSAnaW50ZW50OidcblxuLyoqXG4gKiBHZXQgYSB2YXJpYWJsZSB1bmRlciB0aGlzIHVzZXIncyBzdG9yYWdlXG4gKiBAdGl0bGUgVmFsaWRhdGUgdXNlciBjaG9pY2VcbiAqIEBjYXRlZ29yeSBTa2lsbHNcbiAqIEBoaWRkZW4gdHJ1ZVxuICogQGF1dGhvciBCb3RwcmVzcywgSW5jLlxuICogQHBhcmFtIHtzdHJpbmd9IGRhdGEgLSBUaGUgcGFyYW1ldGVycyBvZiB0aGUgYXZhaWxhYmxlIGNob2ljZXNcbiAqL1xuY29uc3QgdmFsaWRhdGVDaG9pY2UgPSBhc3luYyBkYXRhID0+IHtcbiAgbGV0IGNob2ljZSA9IHVuZGVmaW5lZFxuICBjb25zdCBjb25maWcgPSBhd2FpdCBicC5jb25maWcuZ2V0TW9kdWxlQ29uZmlnRm9yQm90KCdiYXNpYy1za2lsbHMnLCBldmVudC5ib3RJZClcbiAgY29uc3QgbmIgPSBfLmdldChldmVudC5wcmV2aWV3Lm1hdGNoKC9eWyMpLiFdPyhbXFxkXXsxLDJ9KVsjKS4hXT8kLyksICdbMV0nKVxuXG4gIGlmIChjb25maWcubWF0Y2hOdW1iZXJzICYmIG5iKSB7XG4gICAgY29uc3QgaW5kZXggPSBwYXJzZUludChuYikgLSAxXG4gICAgY29uc3QgZWxlbWVudCA9IGF3YWl0IGJwLmNtcy5nZXRDb250ZW50RWxlbWVudChldmVudC5ib3RJZCwgZGF0YS5jb250ZW50SWQpXG4gICAgY2hvaWNlID0gXy5nZXQoZWxlbWVudCwgYGZvcm1EYXRhLmNob2ljZXMuJHtpbmRleH0udmFsdWVgKVxuICB9XG5cbiAgaWYgKCFjaG9pY2UgJiYgY29uZmlnLm1hdGNoTkxVKSB7XG4gICAgY2hvaWNlID0gXy5maW5kS2V5KGRhdGEua2V5d29yZHMsIGtleXdvcmRzID0+IHtcbiAgICAgIGNvbnN0IGludGVudHMgPSBrZXl3b3Jkc1xuICAgICAgICAuZmlsdGVyKHggPT4gKHggfHwgJycpLnRvTG93ZXJDYXNlKCkuc3RhcnRzV2l0aChJTlRFTlRfUFJFRklYKSlcbiAgICAgICAgLm1hcCh4ID0+IHguc3Vic3RyKElOVEVOVF9QUkVGSVgubGVuZ3RoKSlcbiAgICAgIHJldHVybiBfLnNvbWUoaW50ZW50cywgayA9PiBldmVudC5ubHUuaW50ZW50Lm5hbWUgPT09IGspXG4gICAgfSlcbiAgfVxuXG4gIGlmICghY2hvaWNlKSB7XG4gICAgY2hvaWNlID0gXy5maW5kS2V5KGRhdGEua2V5d29yZHMsIGtleXdvcmRzID0+XG4gICAgICBfLnNvbWUoXG4gICAgICAgIGtleXdvcmRzIHx8IFtdLFxuICAgICAgICBrID0+XG4gICAgICAgICAgXy5pbmNsdWRlcygoZXZlbnQucHJldmlldyB8fCAnJykudG9Mb3dlckNhc2UoKSwgKGsgfHwgJycpLnRvTG93ZXJDYXNlKCkpIHx8XG4gICAgICAgICAgKGV2ZW50LnBheWxvYWQgJiYgXy5pbmNsdWRlcyhfLmdldChldmVudCwgJ3BheWxvYWQudGV4dCcsICcnKS50b0xvd2VyQ2FzZSgpLCAoayB8fCAnJykudG9Mb3dlckNhc2UoKSkpXG4gICAgICApXG4gICAgKVxuICB9XG5cbiAgaWYgKGNob2ljZSkge1xuICAgIHRlbXBbJ3NraWxsLWNob2ljZS12YWxpZCddID0gdHJ1ZVxuICAgIHRlbXBbJ3NraWxsLWNob2ljZS1yZXQnXSA9IGNob2ljZVxuICB9IGVsc2Uge1xuICAgIHRlbXBbJ3NraWxsLWNob2ljZS12YWxpZCddID0gZmFsc2VcbiAgfVxufVxuXG5yZXR1cm4gdmFsaWRhdGVDaG9pY2UoYXJncylcbiJdfQ==