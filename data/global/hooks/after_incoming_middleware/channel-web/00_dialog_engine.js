//CHECKSUM:a554e624956898682c910114d41df542aafdf518733740d5648dd10306843949
"use strict";

const messageTypesToDiscard = ['request_start_conversation', 'say_something', 'postback'];

if (messageTypesToDiscard.includes(event.type)) {
  event.setFlag(bp.IO.WellKnownFlags.SKIP_DIALOG_ENGINE, true);

  if (event.type === 'postback') {
    bp.logger.warn(`Just received a postback event: ${event.payload}. 
    To handle these kind of events. you need to create a hook that will process them.
    Please refer to the documentation here: https://botpress.io/docs/build/code/#hooks`);
  }
}

const saySomethingHook = async () => {
  const text = event.payload.text;

  if (event.type === 'say_something' && text && text.length) {
    if (text.startsWith('#!')) {
      /**
       * Sends an existing content element. Event is specified twice, since the first parameters are the
       * element arguments, and the second one is the event destination (required fields: botId, target, threadId, channel)
       */
      const content = await bp.cms.renderElement(text, event, event);
      await bp.events.replyToEvent(event, content);
    } else {
      // Sends a basic text message
      const payloads = await bp.cms.renderElement('builtin_text', {
        text,
        typing: true
      }, event.channel);
      await bp.events.replyToEvent(event, payloads);
    }
  }
};

return saySomethingHook();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIjAwX2RpYWxvZ19lbmdpbmUuanMiXSwibmFtZXMiOlsibWVzc2FnZVR5cGVzVG9EaXNjYXJkIiwiaW5jbHVkZXMiLCJldmVudCIsInR5cGUiLCJzZXRGbGFnIiwiYnAiLCJJTyIsIldlbGxLbm93bkZsYWdzIiwiU0tJUF9ESUFMT0dfRU5HSU5FIiwibG9nZ2VyIiwid2FybiIsInBheWxvYWQiLCJzYXlTb21ldGhpbmdIb29rIiwidGV4dCIsImxlbmd0aCIsInN0YXJ0c1dpdGgiLCJjb250ZW50IiwiY21zIiwicmVuZGVyRWxlbWVudCIsImV2ZW50cyIsInJlcGx5VG9FdmVudCIsInBheWxvYWRzIiwidHlwaW5nIiwiY2hhbm5lbCJdLCJtYXBwaW5ncyI6Ijs7QUFBQSxNQUFNQSxxQkFBcUIsR0FBRyxDQUFDLDRCQUFELEVBQStCLGVBQS9CLEVBQWdELFVBQWhELENBQTlCOztBQUVBLElBQUlBLHFCQUFxQixDQUFDQyxRQUF0QixDQUErQkMsS0FBSyxDQUFDQyxJQUFyQyxDQUFKLEVBQWdEO0FBQzlDRCxFQUFBQSxLQUFLLENBQUNFLE9BQU4sQ0FBY0MsRUFBRSxDQUFDQyxFQUFILENBQU1DLGNBQU4sQ0FBcUJDLGtCQUFuQyxFQUF1RCxJQUF2RDs7QUFFQSxNQUFJTixLQUFLLENBQUNDLElBQU4sS0FBZSxVQUFuQixFQUErQjtBQUM3QkUsSUFBQUEsRUFBRSxDQUFDSSxNQUFILENBQVVDLElBQVYsQ0FBZ0IsbUNBQWtDUixLQUFLLENBQUNTLE9BQVE7O3VGQUFoRTtBQUdEO0FBQ0Y7O0FBRUQsTUFBTUMsZ0JBQWdCLEdBQUcsWUFBWTtBQUNuQyxRQUFNQyxJQUFJLEdBQUdYLEtBQUssQ0FBQ1MsT0FBTixDQUFjRSxJQUEzQjs7QUFFQSxNQUFJWCxLQUFLLENBQUNDLElBQU4sS0FBZSxlQUFmLElBQWtDVSxJQUFsQyxJQUEwQ0EsSUFBSSxDQUFDQyxNQUFuRCxFQUEyRDtBQUN6RCxRQUFJRCxJQUFJLENBQUNFLFVBQUwsQ0FBZ0IsSUFBaEIsQ0FBSixFQUEyQjtBQUN6Qjs7OztBQUlBLFlBQU1DLE9BQU8sR0FBRyxNQUFNWCxFQUFFLENBQUNZLEdBQUgsQ0FBT0MsYUFBUCxDQUFxQkwsSUFBckIsRUFBMkJYLEtBQTNCLEVBQWtDQSxLQUFsQyxDQUF0QjtBQUNBLFlBQU1HLEVBQUUsQ0FBQ2MsTUFBSCxDQUFVQyxZQUFWLENBQXVCbEIsS0FBdkIsRUFBOEJjLE9BQTlCLENBQU47QUFDRCxLQVBELE1BT087QUFDTDtBQUNBLFlBQU1LLFFBQVEsR0FBRyxNQUFNaEIsRUFBRSxDQUFDWSxHQUFILENBQU9DLGFBQVAsQ0FBcUIsY0FBckIsRUFBcUM7QUFBRUwsUUFBQUEsSUFBRjtBQUFRUyxRQUFBQSxNQUFNLEVBQUU7QUFBaEIsT0FBckMsRUFBNkRwQixLQUFLLENBQUNxQixPQUFuRSxDQUF2QjtBQUNBLFlBQU1sQixFQUFFLENBQUNjLE1BQUgsQ0FBVUMsWUFBVixDQUF1QmxCLEtBQXZCLEVBQThCbUIsUUFBOUIsQ0FBTjtBQUNEO0FBQ0Y7QUFDRixDQWpCRDs7QUFtQkEsT0FBT1QsZ0JBQWdCLEVBQXZCIiwic291cmNlUm9vdCI6Ii92YXIvbGliL2plbmtpbnMvd29ya3NwYWNlL2J1aWxkLWxpbnV4L21vZHVsZXMvY2hhbm5lbC13ZWIvc3JjL2JhY2tlbmQiLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBtZXNzYWdlVHlwZXNUb0Rpc2NhcmQgPSBbJ3JlcXVlc3Rfc3RhcnRfY29udmVyc2F0aW9uJywgJ3NheV9zb21ldGhpbmcnLCAncG9zdGJhY2snXVxuXG5pZiAobWVzc2FnZVR5cGVzVG9EaXNjYXJkLmluY2x1ZGVzKGV2ZW50LnR5cGUpKSB7XG4gIGV2ZW50LnNldEZsYWcoYnAuSU8uV2VsbEtub3duRmxhZ3MuU0tJUF9ESUFMT0dfRU5HSU5FLCB0cnVlKVxuXG4gIGlmIChldmVudC50eXBlID09PSAncG9zdGJhY2snKSB7XG4gICAgYnAubG9nZ2VyLndhcm4oYEp1c3QgcmVjZWl2ZWQgYSBwb3N0YmFjayBldmVudDogJHtldmVudC5wYXlsb2FkfS4gXG4gICAgVG8gaGFuZGxlIHRoZXNlIGtpbmQgb2YgZXZlbnRzLiB5b3UgbmVlZCB0byBjcmVhdGUgYSBob29rIHRoYXQgd2lsbCBwcm9jZXNzIHRoZW0uXG4gICAgUGxlYXNlIHJlZmVyIHRvIHRoZSBkb2N1bWVudGF0aW9uIGhlcmU6IGh0dHBzOi8vYm90cHJlc3MuaW8vZG9jcy9idWlsZC9jb2RlLyNob29rc2ApXG4gIH1cbn1cblxuY29uc3Qgc2F5U29tZXRoaW5nSG9vayA9IGFzeW5jICgpID0+IHtcbiAgY29uc3QgdGV4dCA9IGV2ZW50LnBheWxvYWQudGV4dFxuXG4gIGlmIChldmVudC50eXBlID09PSAnc2F5X3NvbWV0aGluZycgJiYgdGV4dCAmJiB0ZXh0Lmxlbmd0aCkge1xuICAgIGlmICh0ZXh0LnN0YXJ0c1dpdGgoJyMhJykpIHtcbiAgICAgIC8qKlxuICAgICAgICogU2VuZHMgYW4gZXhpc3RpbmcgY29udGVudCBlbGVtZW50LiBFdmVudCBpcyBzcGVjaWZpZWQgdHdpY2UsIHNpbmNlIHRoZSBmaXJzdCBwYXJhbWV0ZXJzIGFyZSB0aGVcbiAgICAgICAqIGVsZW1lbnQgYXJndW1lbnRzLCBhbmQgdGhlIHNlY29uZCBvbmUgaXMgdGhlIGV2ZW50IGRlc3RpbmF0aW9uIChyZXF1aXJlZCBmaWVsZHM6IGJvdElkLCB0YXJnZXQsIHRocmVhZElkLCBjaGFubmVsKVxuICAgICAgICovXG4gICAgICBjb25zdCBjb250ZW50ID0gYXdhaXQgYnAuY21zLnJlbmRlckVsZW1lbnQodGV4dCwgZXZlbnQsIGV2ZW50KVxuICAgICAgYXdhaXQgYnAuZXZlbnRzLnJlcGx5VG9FdmVudChldmVudCwgY29udGVudClcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gU2VuZHMgYSBiYXNpYyB0ZXh0IG1lc3NhZ2VcbiAgICAgIGNvbnN0IHBheWxvYWRzID0gYXdhaXQgYnAuY21zLnJlbmRlckVsZW1lbnQoJ2J1aWx0aW5fdGV4dCcsIHsgdGV4dCwgdHlwaW5nOiB0cnVlIH0sIGV2ZW50LmNoYW5uZWwpXG4gICAgICBhd2FpdCBicC5ldmVudHMucmVwbHlUb0V2ZW50KGV2ZW50LCBwYXlsb2FkcylcbiAgICB9XG4gIH1cbn1cblxucmV0dXJuIHNheVNvbWV0aGluZ0hvb2soKVxuIl19