//CHECKSUM:d8693bdeedd85b177d8efd20dfeedb6227277592ad475038856a892de1a6b5cd
"use strict";

const chatOptions = {
  hideWidget: true,
  config: {
    enableReset: true,
    enableTranscriptDownload: true
  }
};
const params = {
  m: 'channel-web',
  v: 'Fullscreen',
  options: JSON.stringify(chatOptions) // Bot will be available at $EXTERNAL_URL/s/$BOT_NAME

};
bp.http.createShortLink(botId, `${process.EXTERNAL_URL}/lite/${botId}/`, params);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIjAwX2NyZWF0ZV9zaG9ydGxpbmsuanMiXSwibmFtZXMiOlsiY2hhdE9wdGlvbnMiLCJoaWRlV2lkZ2V0IiwiY29uZmlnIiwiZW5hYmxlUmVzZXQiLCJlbmFibGVUcmFuc2NyaXB0RG93bmxvYWQiLCJwYXJhbXMiLCJtIiwidiIsIm9wdGlvbnMiLCJKU09OIiwic3RyaW5naWZ5IiwiYnAiLCJodHRwIiwiY3JlYXRlU2hvcnRMaW5rIiwiYm90SWQiLCJwcm9jZXNzIiwiRVhURVJOQUxfVVJMIl0sIm1hcHBpbmdzIjoiOztBQUFBLE1BQU1BLFdBQVcsR0FBRztBQUNsQkMsRUFBQUEsVUFBVSxFQUFFLElBRE07QUFFbEJDLEVBQUFBLE1BQU0sRUFBRTtBQUNOQyxJQUFBQSxXQUFXLEVBQUUsSUFEUDtBQUVOQyxJQUFBQSx3QkFBd0IsRUFBRTtBQUZwQjtBQUZVLENBQXBCO0FBUUEsTUFBTUMsTUFBTSxHQUFHO0FBQ2JDLEVBQUFBLENBQUMsRUFBRSxhQURVO0FBRWJDLEVBQUFBLENBQUMsRUFBRSxZQUZVO0FBR2JDLEVBQUFBLE9BQU8sRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWVWLFdBQWYsQ0FISSxDQU1mOztBQU5lLENBQWY7QUFPQVcsRUFBRSxDQUFDQyxJQUFILENBQVFDLGVBQVIsQ0FBd0JDLEtBQXhCLEVBQWdDLEdBQUVDLE9BQU8sQ0FBQ0MsWUFBYSxTQUFRRixLQUFNLEdBQXJFLEVBQXlFVCxNQUF6RSIsInNvdXJjZVJvb3QiOiIvdmFyL2xpYi9qZW5raW5zL3dvcmtzcGFjZS9idWlsZC1saW51eC9tb2R1bGVzL2J1aWx0aW4vc3JjL2JhY2tlbmQiLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBjaGF0T3B0aW9ucyA9IHtcbiAgaGlkZVdpZGdldDogdHJ1ZSxcbiAgY29uZmlnOiB7XG4gICAgZW5hYmxlUmVzZXQ6IHRydWUsXG4gICAgZW5hYmxlVHJhbnNjcmlwdERvd25sb2FkOiB0cnVlXG4gIH1cbn1cblxuY29uc3QgcGFyYW1zID0ge1xuICBtOiAnY2hhbm5lbC13ZWInLFxuICB2OiAnRnVsbHNjcmVlbicsXG4gIG9wdGlvbnM6IEpTT04uc3RyaW5naWZ5KGNoYXRPcHRpb25zKVxufVxuXG4vLyBCb3Qgd2lsbCBiZSBhdmFpbGFibGUgYXQgJEVYVEVSTkFMX1VSTC9zLyRCT1RfTkFNRVxuYnAuaHR0cC5jcmVhdGVTaG9ydExpbmsoYm90SWQsIGAke3Byb2Nlc3MuRVhURVJOQUxfVVJMfS9saXRlLyR7Ym90SWR9L2AsIHBhcmFtcylcbiJdfQ==